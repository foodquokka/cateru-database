-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2020 at 10:38 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cater`
--

-- --------------------------------------------------------

--
-- Table structure for table `apriori`
--

CREATE TABLE `apriori` (
  `id` int(10) UNSIGNED NOT NULL,
  `menuID` int(10) UNSIGNED NOT NULL,
  `groupNumber` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apriori`
--

INSERT INTO `apriori` (`id`, `menuID`, `groupNumber`, `deleted_at`) VALUES
(1, 2, 1, NULL),
(2, 3, 1, NULL),
(3, 2, 2, NULL),
(4, 5, 2, NULL),
(5, 2, 3, NULL),
(6, 8, 3, NULL),
(7, 2, 4, NULL),
(8, 14, 4, NULL),
(9, 2, 5, NULL),
(10, 20, 5, NULL),
(11, 2, 6, NULL),
(12, 21, 6, NULL),
(13, 3, 7, NULL),
(14, 5, 7, NULL),
(15, 3, 8, NULL),
(16, 8, 8, NULL),
(17, 3, 9, NULL),
(18, 14, 9, NULL),
(19, 3, 10, NULL),
(20, 20, 10, NULL),
(21, 3, 11, NULL),
(22, 21, 11, NULL),
(23, 5, 12, NULL),
(24, 8, 12, NULL),
(25, 5, 13, NULL),
(26, 14, 13, NULL),
(27, 5, 14, NULL),
(28, 20, 14, NULL),
(29, 5, 15, NULL),
(30, 21, 15, NULL),
(31, 8, 16, NULL),
(32, 20, 16, NULL),
(33, 8, 17, NULL),
(34, 21, 17, NULL),
(35, 14, 18, NULL),
(36, 20, 18, NULL),
(37, 14, 19, NULL),
(38, 21, 19, NULL),
(39, 20, 20, NULL),
(40, 21, 20, NULL),
(41, 2, 21, NULL),
(42, 3, 21, NULL),
(43, 5, 21, NULL),
(44, 2, 22, NULL),
(45, 3, 22, NULL),
(46, 8, 22, NULL),
(47, 2, 23, NULL),
(48, 3, 23, NULL),
(49, 14, 23, NULL),
(50, 2, 24, NULL),
(51, 3, 24, NULL),
(52, 20, 24, NULL),
(53, 2, 25, NULL),
(54, 3, 25, NULL),
(55, 21, 25, NULL),
(56, 2, 26, NULL),
(57, 5, 26, NULL),
(58, 8, 26, NULL),
(59, 2, 27, NULL),
(60, 5, 27, NULL),
(61, 20, 27, NULL),
(62, 2, 28, NULL),
(63, 5, 28, NULL),
(64, 21, 28, NULL),
(65, 2, 29, NULL),
(66, 8, 29, NULL),
(67, 20, 29, NULL),
(68, 2, 30, NULL),
(69, 8, 30, NULL),
(70, 21, 30, NULL),
(71, 2, 31, NULL),
(72, 20, 31, NULL),
(73, 21, 31, NULL),
(74, 3, 32, NULL),
(75, 5, 32, NULL),
(76, 8, 32, NULL),
(77, 3, 33, NULL),
(78, 5, 33, NULL),
(79, 14, 33, NULL),
(80, 3, 34, NULL),
(81, 5, 34, NULL),
(82, 20, 34, NULL),
(83, 3, 35, NULL),
(84, 5, 35, NULL),
(85, 21, 35, NULL),
(86, 3, 36, NULL),
(87, 8, 36, NULL),
(88, 20, 36, NULL),
(89, 3, 37, NULL),
(90, 8, 37, NULL),
(91, 21, 37, NULL),
(92, 3, 38, NULL),
(93, 14, 38, NULL),
(94, 20, 38, NULL),
(95, 3, 39, NULL),
(96, 14, 39, NULL),
(97, 21, 39, NULL),
(98, 3, 40, NULL),
(99, 20, 40, NULL),
(100, 21, 40, NULL),
(101, 5, 41, NULL),
(102, 8, 41, NULL),
(103, 20, 41, NULL),
(104, 5, 42, NULL),
(105, 14, 42, NULL),
(106, 20, 42, NULL),
(107, 5, 43, NULL),
(108, 20, 43, NULL),
(109, 21, 43, NULL),
(110, 2, 44, NULL),
(111, 3, 44, NULL),
(112, 5, 44, NULL),
(113, 8, 44, NULL),
(114, 2, 45, NULL),
(115, 3, 45, NULL),
(116, 5, 45, NULL),
(117, 20, 45, NULL),
(118, 2, 46, NULL),
(119, 3, 46, NULL),
(120, 5, 46, NULL),
(121, 21, 46, NULL),
(122, 2, 47, NULL),
(123, 3, 47, NULL),
(124, 8, 47, NULL),
(125, 20, 47, NULL),
(126, 2, 48, NULL),
(127, 3, 48, NULL),
(128, 8, 48, NULL),
(129, 21, 48, NULL),
(130, 2, 49, NULL),
(131, 3, 49, NULL),
(132, 20, 49, NULL),
(133, 21, 49, NULL),
(134, 2, 50, NULL),
(135, 5, 50, NULL),
(136, 8, 50, NULL),
(137, 20, 50, NULL),
(138, 2, 51, NULL),
(139, 5, 51, NULL),
(140, 20, 51, NULL),
(141, 21, 51, NULL),
(142, 3, 52, NULL),
(143, 5, 52, NULL),
(144, 8, 52, NULL),
(145, 20, 52, NULL),
(146, 3, 53, NULL),
(147, 5, 53, NULL),
(148, 14, 53, NULL),
(149, 20, 53, NULL),
(150, 3, 54, NULL),
(151, 5, 54, NULL),
(152, 20, 54, NULL),
(153, 21, 54, NULL),
(154, 2, 55, NULL),
(155, 3, 55, NULL),
(156, 5, 55, NULL),
(157, 8, 55, NULL),
(158, 20, 55, NULL),
(159, 2, 56, NULL),
(160, 3, 56, NULL),
(161, 5, 56, NULL),
(162, 20, 56, NULL),
(163, 21, 56, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `apriorisettings`
--

CREATE TABLE `apriorisettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `support` int(10) UNSIGNED NOT NULL,
  `confidence` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `apriorisettings`
--

INSERT INTO `apriorisettings` (`id`, `support`, `confidence`) VALUES
(1, 80, 80);

-- --------------------------------------------------------

--
-- Table structure for table `bundle_details`
--

CREATE TABLE `bundle_details` (
  `bundle_details_id` int(10) UNSIGNED NOT NULL,
  `menuID` int(10) UNSIGNED NOT NULL,
  `bundleid` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundle_details`
--

INSERT INTO `bundle_details` (`bundle_details_id`, `menuID`, `bundleid`, `qty`, `deleted_at`) VALUES
(1, 3, 1, 3, NULL),
(2, 5, 1, 3, NULL),
(3, 18, 1, 3, NULL),
(4, 24, 1, 3, NULL),
(5, 3, 2, 3, NULL),
(6, 8, 2, 3, NULL),
(7, 19, 2, 3, NULL),
(8, 3, 3, 3, NULL),
(9, 2, 3, 3, NULL),
(10, 17, 3, 3, NULL),
(11, 15, 3, 3, NULL),
(12, 3, 4, 3, NULL),
(13, 14, 4, 3, NULL),
(14, 19, 4, 3, NULL),
(15, 3, 5, 3, NULL),
(16, 16, 5, 3, NULL),
(17, 12, 5, 3, NULL),
(18, 3, 6, 3, NULL),
(19, 20, 6, 3, NULL),
(20, 9, 6, 3, NULL),
(21, 18, 6, 3, NULL),
(22, 3, 7, 3, NULL),
(23, 21, 7, 3, NULL),
(24, 18, 7, 3, NULL),
(25, 24, 7, 3, NULL),
(26, 3, 8, 3, NULL),
(27, 22, 8, 3, NULL),
(28, 18, 8, 3, NULL),
(29, 11, 8, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bundle_menus`
--

CREATE TABLE `bundle_menus` (
  `bundleid` int(10) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL,
  `servingsize` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bundle_menus`
--

INSERT INTO `bundle_menus` (`bundleid`, `price`, `servingsize`, `name`, `image`, `deleted_at`) VALUES
(1, 265.00, 2, 'bundle 1', 'CaterU.png', NULL),
(2, 451.00, 3, 'bundle 2', 'CaterU.png', NULL),
(3, 600.00, 4, 'bundle 3', 'CaterU.png', NULL),
(4, 1500.00, 6, 'bundle 4', 'CaterU.png', NULL),
(5, 1000.00, 5, 'bundle 5', 'CaterU.png', NULL),
(6, 900.00, 3, 'bundle 6', 'CaterU.png', NULL),
(7, 500.00, 4, 'bundle 7', 'CaterU.png', NULL),
(8, 750.00, 5, 'bundle 8', 'CaterU.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `menuID` int(10) UNSIGNED NOT NULL,
  `bundleid` int(10) UNSIGNED DEFAULT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `order_id`, `menuID`, `bundleid`, `qty`) VALUES
(9, 31, 3, NULL, 1),
(10, 31, 8, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryid` int(10) UNSIGNED NOT NULL,
  `categoryname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryid`, `categoryname`, `description`, `deleted_at`) VALUES
(1, 'Barbecue Classics', 'All about Barbecue', NULL),
(2, 'Appetizers', 'Bon Apetit', NULL),
(3, 'Beef', 'Beef Menus', NULL),
(4, 'Pork', 'Loriem Ipsum', NULL),
(5, 'Rice', 'Loriem Ipsum', NULL),
(6, 'Dessert', 'Loriem Ipsum', NULL),
(7, 'Drinks', 'Loriem Ipsum', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `companyname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactNo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `custid` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partysize` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tableno` int(10) UNSIGNED DEFAULT NULL,
  `time_notified` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`custid`, `name`, `phonenumber`, `partysize`, `status`, `tableno`, `time_notified`, `deleted_at`) VALUES
(1, 'Maria', '09479842734', NULL, NULL, NULL, '2020-03-11 14:33:47', NULL),
(2, 'Jay', '09234424688', NULL, NULL, NULL, '2020-03-11 14:33:47', NULL),
(3, 'Rhea', '093688775435', NULL, NULL, NULL, '2020-03-11 14:33:47', NULL),
(4, 'John', '092271094671', NULL, NULL, NULL, '2020-03-11 14:33:47', NULL),
(5, 'Jade', '0944561074257', NULL, NULL, NULL, '2020-03-11 14:33:47', NULL),
(6, 'Rex', '094733578734', NULL, NULL, NULL, '2020-03-11 14:33:47', NULL),
(7, 'Maria', '091590643467', NULL, NULL, NULL, '2020-03-11 14:33:47', NULL),
(8, 'ann', '639479845480', 4, 'cancelled', 6, '2020-03-11 06:44:12', NULL),
(9, 'cash', NULL, NULL, NULL, NULL, '2020-03-11 16:37:08', NULL),
(10, 'cash', NULL, NULL, NULL, NULL, '2020-03-11 17:04:49', NULL),
(11, 'cash', NULL, NULL, NULL, NULL, '2020-03-11 17:06:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `devicelogs`
--

CREATE TABLE `devicelogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `tableno` int(10) UNSIGNED NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loggedin` timestamp NOT NULL DEFAULT current_timestamp(),
  `loggedout` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `empid` int(10) UNSIGNED NOT NULL,
  `empfirstname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emplastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`empid`, `empfirstname`, `emplastname`, `username`, `password`, `image`, `position`, `remember_token`, `deleted_at`) VALUES
(1, 'Myra', 'Pelostratos', 'adminmyra', '$2y$10$LUhcRilVQ2frEAUVySTg0u0c6hz9vtEZmQb8Vkj95DMRVxNPqridq', NULL, 'admin', NULL, NULL),
(2, 'Jose', 'Dela Cruz', 'cashierjose', '$2y$10$VVCXrRRi94R3LHBsyKsu3egqVPeE9VTOMqqUI4nGEIyFL0fpWhImm', NULL, 'cashier', NULL, NULL),
(3, 'Cardo', 'Dalisay', 'waitercardo', '$2y$10$7L/XfSJn1in8nLm0vIUyUulsuI74DuKlLacIyFj4Nu0P2RkbBnwAe', NULL, 'waiter', NULL, NULL),
(4, 'Elaine', 'Perez', 'waiterelaine', '$2y$10$U0zfYnudTrWI04iHj22Ug.OMGUIuuNiBNZbQrFNeM2Fpk06WjgOP.', NULL, 'waiter', NULL, NULL),
(5, 'Richard', 'Gomez', 'receptionistrichard', '$2y$10$zYhO5oaR8jI9uMGZLO.s7O9krFQLBvAj/UdEBKYkGuwm7f3TctDYq', NULL, 'receptionist', NULL, NULL),
(6, 'Tony', 'Estrada', 'watchertony', '$2y$10$2D2z3VTVoVWASJqTzjGGFewBNCIGB4V1vRQ.1UIKSqpJ760gALJDS', NULL, 'watcher', NULL, NULL),
(7, 'alex', 'mercado', 'alexkitchenstaff', '$2y$10$4uyedwkunmzxznGnecv1c.3EGbhAcwLeD20Xud4/.eG2Ww7bbmQiC', NULL, 'kitchenstaff', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kitchenrecords`
--

CREATE TABLE `kitchenrecords` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `menuID` int(10) UNSIGNED NOT NULL,
  `bundleid` int(10) UNSIGNED DEFAULT NULL,
  `orderQty` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_ordered` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `menuID` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) NOT NULL,
  `servingsize` int(11) NOT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subcatid` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`menuID`, `name`, `details`, `price`, `servingsize`, `image`, `subcatid`, `deleted_at`) VALUES
(2, '3 pcs. Chicken Barbecue', '3 pcs. of Aristocrat\'s, most popular dish, Chicken Barbecue w/ Java Rice and Java Sauce * A la carte (no rice) option also available ** P 10 for extra Java Sauce.', 215.00, 1, '3pcs_Chicken_Barbecue3.png', 1, NULL),
(3, 'Boneless Chicken Barbecue', 'Boneless (BLESS) Chicken Barbecue w/ Java Rice and Java Sauce * A la carte (no rice) option also available **\r\n                P 10 for extra Java Sauce.', 195.00, 3, 'Boneless_Chicken_Barbecue3.png', 1, NULL),
(5, 'Pork Barbecue', '2 sticks of Aristocrat\'s flavorful Pork Barbecue w/ Java RIce and Java Sauce * A la carte (no rice) option also available ** P 10 for extra Java Sauce.', 180.00, 1, 'Pork_Barbecue3.png', 5, NULL),
(8, 'Pork Spareribs Barbecue (whole/half)', 'Grilled lean and tender choice cut pork ribs', 359.00, 2, 'Pork_Spareribs3.png', 5, NULL),
(9, 'Calamares', 'Batter-coated and deep-fried squid served with tartar sauce', 297.00, 2, 'Calamares3.png', 3, NULL),
(10, 'Sizzling Gambas', 'Shrimp in a special blend of spices, sprinkled with roasted garlic.', 323.00, 2, 'Sizzling_Gambas3.png', 3, NULL),
(11, 'Camaron Rebosado', 'Deep-fried battered shrimp served with sweet and sour sauce.', 338.00, 2, 'Camaron_Rebosado3.png', 3, NULL),
(12, 'Kare-Kare', '3 pcs. of Aristocrat\'s, most popular dish, Chicken Barbecue w/ Java Rice and Java Sauce * A la carte (no rice) option also available ** P 10 for extra Java Sauce.', 474.00, 4, 'Kare-Kare3.png', 4, NULL),
(13, 'Bulalo', 'Slowly cooked, rich, beef soup with bone marrow and vegetables.', 479.00, 4, 'Bulalo3.png', 4, NULL),
(14, 'Crispy Pata', 'Deep fried leg of pork, with homemade atcahara', 755.00, 4, 'Crispy_Pata3.jpg', 5, NULL),
(15, 'Sinigang na Baboy', 'Pork spareribs with native vegetables in sour tamarind soup.', 338.00, 4, 'SinigangnaBaboy3.jpg', 5, NULL),
(16, 'Plain Rice', '', 37.00, 3, 'Plain_Rice3.png', 6, NULL),
(17, 'Shanghai Rice', 'Fried rice with pork, shrimp, ham, sausage, and egg.', 219.00, 3, 'Shanghai_Rice3.png', 7, NULL),
(18, 'Aristocrat Java Rice', 'Signature fried rice of the restaurant pairs perfectly with any of the barbecue dishes.', 47.00, 1, 'Java_Rice3.png', 7, NULL),
(19, 'Ice Cream', 'Scoop of Ube, Vanilla, Strawberry, or Chocolate.', 58.00, 1, 'ice-cream.jpg', 8, NULL),
(20, 'Banana Split', 'Three (3) scoops opf ice cream, with chocolate, caramel, and strawberry syrup whipped cream, and nuts.', 265.00, 3, 'banana-split.png', 8, NULL),
(21, 'Fresh Fruit Salad', 'Also Available: Fruit per slice-Mango,Papaya, and Bananas.', 193.00, 1, 'Fruit_Salad3.png', 9, NULL),
(22, 'House Blend Ice Tea', '', 73.00, 1, 'colddrinks2.png', 10, NULL),
(23, 'Iced Coffee', '', 63.00, 1, 'colddrinks2.png', 10, NULL),
(24, 'Beer', '', 99.00, 1, 'colddrinks2.png', 11, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(6, '2019_05_10_005122_create_employee_table', 1),
(7, '2019_05_14_133319_create_categories_table', 1),
(8, '2019_05_15_000020_sub_category', 1),
(9, '2019_05_19_055958_create_tables_table', 1),
(10, '2019_05_19_143200_create_menus_table', 1),
(11, '2019_05_19_143356_create_bundle_menus_table', 1),
(12, '2019_05_19_143649_create_customers_table', 1),
(13, '2019_05_19_143655_create_orders_table', 1),
(14, '2019_05_20_143654_create_order_details_table', 1),
(15, '2019_10_23_014709_create_sessions_table', 1),
(16, '2019_10_23_064948_devicelogs', 1),
(17, '2019_11_13_063316_temporary_cart_table', 1),
(18, '2019_12_13_103910_create_apriori_settings_table', 1),
(19, '2019_12_19_145825_create_apriori_table', 1),
(20, '2020_01_20_143313_kitchen', 1),
(21, '2020_01_29_071705_create_bundle_details', 1),
(22, '2020_02_24_214215_companies', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('1b5848a69bab9f1fb77ebcb780b6d76cecaac3766df4cdd19c25954167240e9d6606353f2a4d7262', 5, 1, 'MyApp', '[]', 0, '2020-03-11 07:49:21', '2020-03-11 07:49:21', '2021-03-11 15:49:21'),
('4109b22d66ec9c81cc2694b242aeda5d74ca47921d88ecdbddbdd310d7e34d0f4507e0e3158b45f4', 4, 1, 'MyApp', '[]', 0, '2020-03-11 08:37:33', '2020-03-11 08:37:33', '2021-03-11 16:37:33'),
('e5f6b22bab6d4a933da00120a3e91656b9c86afee2aef96390e9910b6c3e8ac932b2e3c793e587d5', 4, 1, 'MyApp', '[]', 0, '2020-03-11 08:36:46', '2020-03-11 08:36:46', '2021-03-11 16:36:46');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'CaterU Personal Access Client', 'HfvsaJW2TObc34WF62wRjaoL73Hx17s0tuRvgwmv', 'http://localhost', 1, 0, 0, '2020-03-11 06:33:46', '2020-03-11 06:33:46'),
(2, NULL, 'CaterU Password Grant Client', 'KD1FZhy4pNLNd8lpI5P6yRrer9CDUn3Q6GmTvhCu', 'http://localhost', 0, 1, 0, '2020-03-11 06:33:46', '2020-03-11 06:33:46');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-03-11 06:33:46', '2020-03-11 06:33:46');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `custid` int(10) UNSIGNED DEFAULT NULL,
  `empid` int(10) UNSIGNED NOT NULL,
  `tableno` int(10) UNSIGNED NOT NULL,
  `discountType` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `discountedTotal` double(8,2) DEFAULT NULL,
  `total` double(8,2) NOT NULL,
  `vatExemptRate` double(8,2) DEFAULT NULL,
  `VATableSales` double(8,2) DEFAULT NULL,
  `vat` double(8,2) DEFAULT NULL,
  `cashTender` double(8,2) DEFAULT NULL,
  `change` double(8,2) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_ordered` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `custid`, `empid`, `tableno`, `discountType`, `discount`, `discountedTotal`, `total`, `vatExemptRate`, `VATableSales`, `vat`, `cashTender`, `change`, `status`, `date_ordered`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 2, NULL, NULL, NULL, 2509.00, NULL, NULL, NULL, 2510.00, 1.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(2, 2, 2, 1, NULL, NULL, NULL, 3748.00, NULL, NULL, NULL, 4000.00, 252.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(3, 3, 3, 4, NULL, NULL, NULL, 3679.00, NULL, NULL, NULL, 3700.00, 21.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(4, 1, 1, 5, NULL, NULL, NULL, 410.00, NULL, NULL, NULL, 500.00, 90.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(5, 2, 3, 6, NULL, NULL, NULL, 3352.00, NULL, NULL, NULL, 3400.00, 48.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(6, 3, 2, 6, NULL, NULL, NULL, 3130.00, NULL, NULL, NULL, 3200.00, 70.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(7, 1, 6, 5, NULL, NULL, NULL, 3600.00, NULL, NULL, NULL, 3600.00, 0.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(8, 2, 5, 1, NULL, NULL, NULL, 2638.00, NULL, NULL, NULL, 3000.00, 362.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(9, 3, 2, 3, NULL, NULL, NULL, 525.00, NULL, NULL, NULL, 550.00, 25.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(10, 1, 3, 4, NULL, NULL, NULL, 2513.00, NULL, NULL, NULL, 2520.00, 7.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(11, 2, 1, 1, NULL, NULL, NULL, 3438.00, NULL, NULL, NULL, 3500.00, 62.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(12, 3, 4, 5, NULL, NULL, NULL, 3238.00, NULL, NULL, NULL, 3240.00, 2.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(13, 1, 6, 6, NULL, NULL, NULL, 3970.00, NULL, NULL, NULL, 4000.00, 30.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(14, 2, 1, 3, NULL, NULL, NULL, 3455.00, NULL, NULL, NULL, 3460.00, 5.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(15, 3, 3, 4, NULL, NULL, NULL, 3688.00, NULL, NULL, NULL, 3700.00, 22.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(16, 1, 2, 6, NULL, NULL, NULL, 3866.00, NULL, NULL, NULL, 3900.00, 34.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(17, 2, 3, 1, NULL, NULL, NULL, 3736.00, NULL, NULL, NULL, 3740.00, 4.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(18, 3, 5, 2, NULL, NULL, NULL, 3751.00, NULL, NULL, NULL, 3800.00, 49.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(19, 1, 2, 3, NULL, NULL, NULL, 3309.00, NULL, NULL, NULL, 3310.00, 1.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(20, 2, 3, 4, NULL, NULL, NULL, 485.00, NULL, NULL, NULL, 490.00, 5.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(21, 1, 1, 2, NULL, NULL, NULL, 3632.00, NULL, NULL, NULL, 3640.00, 8.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(22, 2, 2, 1, NULL, NULL, NULL, 3991.00, NULL, NULL, NULL, 4000.00, 9.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(23, 3, 3, 4, NULL, NULL, NULL, 2876.00, NULL, NULL, NULL, 2900.00, 24.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(24, 1, 1, 5, NULL, NULL, NULL, 2845.00, NULL, NULL, NULL, 2850.00, 5.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(25, 2, 3, 6, NULL, NULL, NULL, 3628.00, NULL, NULL, NULL, 3630.00, 2.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(26, 3, 2, 6, NULL, NULL, NULL, 3414.00, NULL, NULL, NULL, 3420.00, 6.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(27, 1, 6, 5, NULL, NULL, NULL, 2580.00, NULL, NULL, NULL, 2600.00, 20.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(28, 2, 5, 1, NULL, NULL, NULL, 2427.00, NULL, NULL, NULL, 2430.00, 3.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(29, 3, 2, 3, NULL, NULL, NULL, 3433.00, NULL, NULL, NULL, 3440.00, 7.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(30, 1, 3, 4, NULL, NULL, NULL, 3080.00, NULL, NULL, NULL, 3100.00, 20.00, 'paid', '2020-03-11 14:34:42', '2020-03-11 14:34:42', NULL),
(31, 9, 4, 1, NULL, NULL, NULL, 15201.00, NULL, NULL, NULL, NULL, NULL, 'ordering', '2020-03-11 16:37:08', '2020-03-12 05:30:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `orderQty` int(11) NOT NULL,
  `menuID` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` double(8,2) NOT NULL,
  `date_ordered` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `order_id` int(10) UNSIGNED NOT NULL,
  `qtyServed` int(10) UNSIGNED DEFAULT NULL,
  `bundleid` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `FLAG` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `orderQty`, `menuID`, `status`, `subtotal`, `date_ordered`, `updated_at`, `order_id`, `qtyServed`, `bundleid`, `deleted_at`, `FLAG`) VALUES
(1, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(2, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(3, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(4, 1, 9, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(5, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(6, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(7, 1, 15, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(8, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(9, 1, 19, 'served', 58.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(10, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(11, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 6, NULL, NULL, NULL, NULL),
(12, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(13, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(14, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(15, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(16, 1, 9, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(17, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(18, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(19, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(20, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(21, 1, 20, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(22, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 7, NULL, NULL, NULL, NULL),
(23, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(24, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(25, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(26, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(27, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(28, 1, 15, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(29, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(30, 1, 19, 'served', 58.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(31, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(32, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 8, NULL, NULL, NULL, NULL),
(33, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(34, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(35, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(36, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(37, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(38, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(39, 1, 19, 'served', 58.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(40, 1, 20, 'served', 268.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(41, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 9, NULL, NULL, NULL, NULL),
(42, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(43, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(44, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(45, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(46, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(47, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(48, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(49, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(50, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(51, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(52, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(53, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(54, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 10, NULL, NULL, NULL, NULL),
(55, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(56, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(57, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(58, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(59, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(60, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(61, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(62, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(63, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(64, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(65, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 11, NULL, NULL, NULL, NULL),
(66, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(67, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(68, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(69, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(70, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(71, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(72, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(73, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(74, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(75, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(76, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 12, NULL, NULL, NULL, NULL),
(77, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(78, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(79, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(80, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(81, 1, 9, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(82, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(83, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(84, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(85, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(86, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(87, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(88, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(89, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 13, NULL, NULL, NULL, NULL),
(90, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(91, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(92, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(93, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(94, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(95, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(96, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(97, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(98, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(99, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 14, NULL, NULL, NULL, NULL),
(100, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(101, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(102, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(103, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(104, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(105, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(106, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(107, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(108, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(109, 1, 19, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(110, 1, 20, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(111, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 15, NULL, NULL, NULL, NULL),
(112, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(113, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(114, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(115, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(116, 1, 9, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(117, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(118, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(119, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(120, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(121, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(122, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(123, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(124, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 16, NULL, NULL, NULL, NULL),
(125, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(126, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(127, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(128, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(129, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(130, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(131, 1, 12, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(132, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(133, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(134, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(135, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(136, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 17, NULL, NULL, NULL, NULL),
(137, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(138, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(139, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(140, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(141, 1, 9, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(142, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(143, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(144, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(145, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(146, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(147, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(148, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 18, NULL, NULL, NULL, NULL),
(149, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(150, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(151, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(152, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(153, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(154, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(155, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(156, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(157, 1, 19, 'served', 58.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(158, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(159, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(160, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 19, NULL, NULL, NULL, NULL),
(161, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(162, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(163, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(164, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(165, 1, 9, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(166, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(167, 1, 12, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(168, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(169, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(170, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(171, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(172, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 20, NULL, NULL, NULL, NULL),
(173, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(174, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(175, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(176, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(177, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(178, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(179, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(180, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(181, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(182, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(183, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(184, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 21, NULL, NULL, NULL, NULL),
(185, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(186, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(187, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(188, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(189, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(190, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(191, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(192, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(193, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(194, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(195, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(196, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(197, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(198, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 22, NULL, NULL, NULL, NULL),
(199, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(200, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(201, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(202, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(203, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(204, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(205, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(206, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(207, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(208, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 23, NULL, NULL, NULL, NULL),
(209, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(210, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(211, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(212, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(213, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(214, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(215, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(216, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(217, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 2, NULL, NULL, NULL, NULL),
(218, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(219, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(220, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(221, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(222, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(223, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(224, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(225, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(226, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(227, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(228, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(229, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 25, NULL, NULL, NULL, NULL),
(230, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(231, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(232, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(233, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(234, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(235, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(236, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(237, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(238, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(239, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(240, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 26, NULL, NULL, NULL, NULL),
(241, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(242, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(243, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(244, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(245, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(246, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(247, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(248, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(249, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(250, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 27, NULL, NULL, NULL, NULL),
(251, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(252, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(253, 1, 9, 'served', 297.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(254, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(255, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(256, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(257, 1, 19, 'served', 58.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(258, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(259, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 28, NULL, NULL, NULL, NULL),
(260, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(261, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(262, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(263, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(264, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(265, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(266, 1, 13, 'served', 479.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(267, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(268, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(269, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(270, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(271, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 29, NULL, NULL, NULL, NULL),
(272, 1, 2, 'served', 215.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(273, 1, 3, 'served', 195.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(274, 1, 5, 'served', 180.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(275, 1, 8, 'served', 359.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(276, 1, 10, 'served', 323.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(277, 1, 11, 'served', 338.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(278, 1, 12, 'served', 474.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(279, 1, 14, 'served', 755.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(280, 1, 17, 'served', 219.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(281, 1, 18, 'served', 47.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(282, 1, 20, 'served', 265.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(283, 1, 21, 'served', 193.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL),
(284, 1, 2, 'served', 99.00, '2020-03-11 14:34:44', '2020-03-11 14:34:44', 30, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('9jEajB015Iu2LBEDynsYNGL40QhEM87dVQRa3UBP', NULL, '192.168.101.5', 'Mozilla/5.0 (Linux; Android 8.1.0; Lenovo TB-X304L Build/OPM1.171019.026; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/79.0.3945.136 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiUFI0eWN6c2FXY1V6bVFWWDYyNGRMblRocXNlNU5wZFo1RUNNbEpCUCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1583944577),
('9uKWeDl7sUPPX5Eq3VAijESOe3VVSKRiCxljeXLY', 4, '192.168.101.5', 'Mozilla/5.0 (Linux; Android 8.1.0; Lenovo TB-X304L Build/OPM1.171019.026; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/79.0.3945.136 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoidkdWbzJTbFRhTzN6TXNHd1N1ZnV0RzI4UzZoeEdsc0FBWDhSV1pJOCI7czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6NDtzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1583944606),
('QZzBlASeY749svvEJ1qjo7uH8OnQe1nlgTq6UOZV', 4, '192.168.101.5', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiTWVqSjNDQTU1ZlBEWWVpVXlJTEhDWTB2U0FGQXZJQUJ5UWxXTGZ1eCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzA6Imh0dHA6Ly8xOTIuMTY4LjEwMS41L2Rhc2hib2FyZCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjQ7fQ==', 1583944676),
('rom9kCXGgf129ubM2KeoCbTqnlKEAtBa0xHRop2j', 5, '192.168.101.5', 'Mozilla/5.0 (Linux; Android 8.1.0; Lenovo TB-X304L Build/OPM1.171019.026; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/79.0.3945.136 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiRVJYZ1VKNlJzOFpOVmVIblZDbUtIRTJZcjZEZWMxWHYxQUpOdXhDNyI7czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6NTtzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1583941761),
('ShoPcgMBvIkFcHDKd8DI4kRa8VrQETycSCJOTsRe', NULL, '192.168.101.5', 'Mozilla/5.0 (Linux; Android 8.1.0; Lenovo TB-X304L Build/OPM1.171019.026; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/79.0.3945.136 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoidktUM3QzNDZnU0FPT1cyMkEzQ3J5eFAwa2U0SUx6aU1HanNnZ2ZuRSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1583944575);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `subcatid` int(10) UNSIGNED NOT NULL,
  `subname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryid` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`subcatid`, `subname`, `categoryid`, `deleted_at`) VALUES
(1, 'CHICKEN', 1, NULL),
(3, 'SEAFOOD', 2, NULL),
(4, 'BEEF', 3, NULL),
(5, 'PORK', 4, NULL),
(6, 'PLAIN', 5, NULL),
(7, 'FRIED', 5, NULL),
(8, 'ICE CREAM', 6, NULL),
(9, 'SALAD', 6, NULL),
(10, 'NON ALOCHOLIC', 7, NULL),
(11, 'ALCOHOLIC', 7, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `tableno` int(10) UNSIGNED NOT NULL,
  `capacity` int(11) NOT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concern` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`tableno`, `capacity`, `status`, `concern`, `deleted_at`) VALUES
(1, 4, 'Occupied', NULL, NULL),
(2, 6, 'Occupied', NULL, NULL),
(3, 2, 'Available', NULL, NULL),
(4, 4, 'Available', NULL, NULL),
(5, 6, 'Available', NULL, NULL),
(6, 2, 'Available', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apriori`
--
ALTER TABLE `apriori`
  ADD PRIMARY KEY (`id`),
  ADD KEY `apriori_menuid_foreign` (`menuID`);

--
-- Indexes for table `apriorisettings`
--
ALTER TABLE `apriorisettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bundle_details`
--
ALTER TABLE `bundle_details`
  ADD PRIMARY KEY (`bundle_details_id`),
  ADD KEY `bundle_details_menuid_foreign` (`menuID`),
  ADD KEY `bundle_details_bundleid_foreign` (`bundleid`);

--
-- Indexes for table `bundle_menus`
--
ALTER TABLE `bundle_menus`
  ADD PRIMARY KEY (`bundleid`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_order_id_foreign` (`order_id`),
  ADD KEY `carts_menuid_foreign` (`menuID`),
  ADD KEY `carts_bundleid_foreign` (`bundleid`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryid`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`custid`),
  ADD KEY `customers_tableno_foreign` (`tableno`);

--
-- Indexes for table `devicelogs`
--
ALTER TABLE `devicelogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devicelogs_tableno_foreign` (`tableno`),
  ADD KEY `devicelogs_session_id_foreign` (`session_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`empid`);

--
-- Indexes for table `kitchenrecords`
--
ALTER TABLE `kitchenrecords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kitchenrecords_order_id_foreign` (`order_id`),
  ADD KEY `kitchenrecords_menuid_foreign` (`menuID`),
  ADD KEY `kitchenrecords_bundleid_foreign` (`bundleid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`menuID`),
  ADD KEY `menus_subcatid_foreign` (`subcatid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `orders_custid_foreign` (`custid`),
  ADD KEY `orders_empid_foreign` (`empid`),
  ADD KEY `orders_tableno_foreign` (`tableno`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`),
  ADD KEY `order_details_menuid_foreign` (`menuID`),
  ADD KEY `order_details_bundleid_foreign` (`bundleid`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`subcatid`),
  ADD KEY `sub_categories_categoryid_foreign` (`categoryid`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`tableno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apriori`
--
ALTER TABLE `apriori`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `apriorisettings`
--
ALTER TABLE `apriorisettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bundle_details`
--
ALTER TABLE `bundle_details`
  MODIFY `bundle_details_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `custid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `devicelogs`
--
ALTER TABLE `devicelogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `empid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kitchenrecords`
--
ALTER TABLE `kitchenrecords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `menuID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `subcatid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `apriori`
--
ALTER TABLE `apriori`
  ADD CONSTRAINT `apriori_menuid_foreign` FOREIGN KEY (`menuID`) REFERENCES `menus` (`menuID`) ON UPDATE CASCADE;

--
-- Constraints for table `bundle_details`
--
ALTER TABLE `bundle_details`
  ADD CONSTRAINT `bundle_details_bundleid_foreign` FOREIGN KEY (`bundleid`) REFERENCES `bundle_menus` (`bundleid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `bundle_details_menuid_foreign` FOREIGN KEY (`menuID`) REFERENCES `menus` (`menuID`) ON UPDATE CASCADE;

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_bundleid_foreign` FOREIGN KEY (`bundleid`) REFERENCES `bundle_menus` (`bundleid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_menuid_foreign` FOREIGN KEY (`menuID`) REFERENCES `menus` (`menuID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON UPDATE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_tableno_foreign` FOREIGN KEY (`tableno`) REFERENCES `tables` (`tableno`) ON UPDATE CASCADE;

--
-- Constraints for table `devicelogs`
--
ALTER TABLE `devicelogs`
  ADD CONSTRAINT `devicelogs_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `devicelogs_tableno_foreign` FOREIGN KEY (`tableno`) REFERENCES `tables` (`tableno`) ON UPDATE CASCADE;

--
-- Constraints for table `kitchenrecords`
--
ALTER TABLE `kitchenrecords`
  ADD CONSTRAINT `kitchenrecords_bundleid_foreign` FOREIGN KEY (`bundleid`) REFERENCES `bundle_menus` (`bundleid`),
  ADD CONSTRAINT `kitchenrecords_menuid_foreign` FOREIGN KEY (`menuID`) REFERENCES `menus` (`menuID`),
  ADD CONSTRAINT `kitchenrecords_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`);

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_subcatid_foreign` FOREIGN KEY (`subcatid`) REFERENCES `sub_categories` (`subcatid`) ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_custid_foreign` FOREIGN KEY (`custid`) REFERENCES `customers` (`custid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_empid_foreign` FOREIGN KEY (`empid`) REFERENCES `employees` (`empid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_tableno_foreign` FOREIGN KEY (`tableno`) REFERENCES `tables` (`tableno`) ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_bundleid_foreign` FOREIGN KEY (`bundleid`) REFERENCES `bundle_menus` (`bundleid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_menuid_foreign` FOREIGN KEY (`menuID`) REFERENCES `menus` (`menuID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_categoryid_foreign` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`categoryid`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
